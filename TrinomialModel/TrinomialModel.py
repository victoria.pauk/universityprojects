#!/usr/bin/env python
# coding: utf-8

import math
import numpy as np
import pandas as pd


class TrinomialOption(object):
    def __init__(self, S0, K, r, T, N, sigma, mu=0, is_call=True, is_am=False, lmbd=1.2):
        self.S0 = S0
        self.K = K
        self.T = T
        self.N = max(1,N)
        self.STs = []
        self.POs=[]
        self.deltas=[]
        self.result_tree=[]

        self.is_call = is_call
        self.is_am = is_am
        self.dt=self.T/float(self.N)
        
        self.sigma = sigma

        self.r=r if mu==0 else  mu+self.sigma*self.sigma/2
        self.u = math.exp(self.sigma*math.sqrt(lmbd*self.dt))
        self.d = 1/self.u
        self.m=1
        
        self._M=math.exp(self.r*self.dt)
        self._V=math.exp(self.sigma*self.sigma*self.dt)
        self.pu=(self._M*self._M*self._V-self._M*(self.d+1)+self.d)/((self.u-self.d)*(self.u-1))
        self.pd=(self._M*self._M*self._V-self._M*(self.u+1)+self.u)/((self.d-self.u)*(self.d-1))
        self.pm = 1 - self.pu - self.pd
        
        
    def check_early_exercise(self, payoffs, node):
        if self.is_call:
            return np.maximum(payoffs, self.STs[node] - self.K)
        else:
            return np.maximum(payoffs, self.K - self.STs[node])
        
    def init_payoffs_tree(self):
        if self.is_call:
            return np.maximum(0, self.STs[self.N]-self.K)
        else:
            return np.maximum(0, self.K-self.STs[self.N])
        
    def init_stock_price_tree(self):
        self.STs = [np.array([self.S0])]

        for i in range(self.N):
            prev_branches = self.STs[-1]
            self.ST = np.concatenate(
                (prev_branches*self.u, 
                 [prev_branches[-1]*self.m, prev_branches[-1]*self.d]))
            self.STs.append(self.ST)

    def traverse_tree(self, payoffs):
        for i in reversed(range(self.N)):
            self.POs.insert(0,payoffs)
            payoffs = (payoffs[:-2] * self.pu +
                       payoffs[1:-1] * self.pm +
                       payoffs[2:] * self.pd) * math.exp(-(self.r)*self.dt) 
            
            if self.is_am:
                payoffs = self.check_early_exercise(payoffs,i)
        self.POs.insert(0,payoffs)
        return payoffs
    
    def print_tree(self):
        self.result_tree=pd.DataFrame(self.STs).fillna('').T
        for k in range(len(self.result_tree.columns)-1):
            self.result_tree.loc[:,:k]=self.result_tree.loc[:,:k].shift(1,fill_value='')
        res=self.result_tree
        print(res.to_string(header=True,index=False))
        
    def print_payoffs(self):
        result=pd.DataFrame(self.POs).fillna('').T
        for k in range(len(result.columns)-1):
            result.loc[:,:k]=result.loc[:,:k].shift(1,fill_value='')
        print(result.to_string(header=True,index=False))
    
    def delta(self,S):
        dS=S/10
        opt1=TrinomialOption(S+dS, self.K, self.r, self.T, self.N, self.sigma,0, self.is_call, self.is_am)
        opt2=TrinomialOption(S-dS, self.K, self.r, self.T, self.N, self.sigma,0, self.is_call, self.is_am)
        return (opt1.price()-opt2.price())/dS/2
        
    def gamma(self,S):
        dS=S/10
        c_S0=TrinomialOption(S, self.K, self.r, self.T, self.N, self.sigma,0, self.is_call, self.is_am).price()
        c_positive_dS=TrinomialOption(S+dS, self.K, self.r, self.T, self.N, self.sigma,0, self.is_call, self.is_am).price()
        c_negative_dS=TrinomialOption(S-dS, self.K, self.r, self.T, self.N, self.sigma,0, self.is_call, self.is_am).price()
        return (c_positive_dS-2*c_S0+c_negative_dS)/(dS*dS)
    
    
    def print_deltas(self):
        deltas_tree=pd.DataFrame(self.STs).fillna(0).T.apply(lambda x: self.delta(x))
        for k in range(len(deltas_tree.columns)-1):
            deltas_tree.loc[:,:k]=deltas_tree.loc[:,:k].shift(1,fill_value='')
        deltas_tree.fillna('',inplace=True)
        print(deltas_tree.to_string(header=True,index=False))
        
    def print_gammas(self):
        gammas_tree=pd.DataFrame(self.STs).fillna(0).T.apply(lambda x: self.gamma(x))
        for k in range(len(gammas_tree.columns)-1):
            gammas_tree.loc[:,:k]=gammas_tree.loc[:,:k].shift(1,fill_value='')
        gammas_tree.fillna('',inplace=True)
        print(gammas_tree.to_string(header=True,index=False))
    
    def delta_hedging(self,n,S_list,K,rate,T,N,sigma,is_call,is_am):
        delta_list=[]
        option=TrinomialOption(S_list[0], K, r=rate, T=T, N=1,sigma=sigma, mu=0, is_call=is_call, is_am=is_am)
        delta_list.append(option.delta(S_list[0]))
        shares=[]
        shares.append(delta_list[0]*100)
        shares_cost=[]
        shares_cost.append(shares[0]*S_list[0])
        cum_cost=[]
        cum_cost.append(shares_cost[0])
    
        interest_cost=[]
        delta_rate=rate*7/365
        interest_cost.append(shares_cost[0]*delta_rate)
        for i in range(1,n):
            option=TrinomialOption(S_list[i], K, r=rate, T=T, N=1,sigma=sigma, mu=0, is_call=is_call, is_am=is_am)
            delta_list.append(option.delta(S_list[i]))
            shares.append((delta_list[i]-delta_list[i-1])*100)
            shares_cost.append(shares[-1]*float(S_list[i]))
            cum_cost.append(cum_cost[-1]+shares_cost[i])
            interest_cost.append(interest_cost[-1]+shares_cost[-1]*delta_rate)
        delta_set=pd.DataFrame({"Stock price":S_list[:n],"Delta":delta_list,"Shares":shares,"Shares cost":shares_cost,"Cumulative cost":cum_cost})#,"Interest cost":interest_cost})
        return delta_set   
    
    def price(self,print_tree=False,print_payoffs=False):
        self.init_stock_price_tree()
        payoffs = self.init_payoffs_tree()
        payoffs = self.traverse_tree(payoffs)
        
        if print_tree:
            self.print_tree()
            print()
        if print_payoffs:
            self.print_payoffs()
            print()
        #self.print_deltas()
        return payoffs[0]