#!/usr/bin/env python
# coding: utf-8

import math
import numpy as np
import pandas as pd
from scipy import stats

class BinomialOption(object):
    def __init__(self, S0, K, r, T, N, sigma, pu=0.5, mu=0, is_call=True, is_am=False):
        self.S0 = S0
        self.K = K
        self.T = T
        self.N = max(1,N)
        self.STs = []
        self.POs=[]

        self.is_call = is_call
        self.is_am = is_am
        self.dt=self.T/float(self.N)
        
        self.sigma = sigma
        self.r=r if mu==0 else  mu+self.sigma*self.sigma/2
        self.u = math.exp(self.sigma*math.sqrt(self.dt))
        self.d = 1/self.u
        self.pu = (math.exp(self.r*self.dt)-self.d)/(self.u-self.d)
        self.pd = 1-self.pu
        
        
    def check_early_exercise(self, payoffs, node):
        if self.is_call:
            return np.maximum(payoffs, self.STs[node] - self.K)
        else:
            return np.maximum(payoffs, self.K - self.STs[node])
        
    def init_payoffs_tree(self):
        if self.is_call:
            return np.maximum(0, self.STs[self.N]-self.K)
        else:
            return np.maximum(0, self.K-self.STs[self.N])
        
    def init_stock_price_tree(self):
        self.STs = [np.array([self.S0])]

        for i in range(self.N):
            prev_branches = self.STs[-1]
            st = np.concatenate(
                (prev_branches*self.u, 
                 [prev_branches[-1]*self.d]))
            self.STs.append(st)
        


    def traverse_tree(self, payoffs):
        for i in reversed(range(self.N)):
            self.POs.insert(0,payoffs)
            payoffs = (payoffs[:-1]*self.pu + 
                       payoffs[1:]*self.pd)*math.exp(-self.r*self.dt)

            if self.is_am:
                payoffs = self.check_early_exercise(payoffs,i)
        self.POs.insert(0,payoffs)
        return payoffs
    
    def print_tree(self):
        result=pd.DataFrame(self.STs).fillna('').T
        for k in range(len(result.columns)-1):
            result.loc[:,:k]=result.loc[:,:k].shift(1,fill_value='')
        print(result.to_string(header=True,index=False))
        
    def print_payoffs(self):
        result=pd.DataFrame(self.POs).fillna('').T
        for k in range(len(result.columns)-1):
            result.loc[:,:k]=result.loc[:,:k].shift(1,fill_value='')
        print(result.to_string(header=True,index=False))
    
    def delta(self,S):
        d1 = (np.log(S/self.K) + (self.r + self.sigma**2/2)*self.dt) / (self.sigma*np.sqrt(self.dt))
        if self.is_call:
            return stats.norm.cdf(d1)
        return stats.norm.cdf(d1)-1
        
    def gamma(self,S):
        d1 = (np.log(S/self.K) + (self.r + self.sigma**2/2)*self.dt) / (self.sigma*np.sqrt(self.dt))
        d2 = d1 - self.sigma * np.sqrt(self.dt)
        return self.K*np.exp(-self.r*self.dt)/S/S/self.sigma/math.sqrt(2*math.pi*self.dt)*np.exp(-d2*d2/2)
    
    def price(self,print_tree=False,print_payoffs=False):
        self.init_stock_price_tree()
        payoffs = self.init_payoffs_tree()
        payoffs = self.traverse_tree(payoffs)
        if print_tree:
            self.print_tree()
            print()
        if print_payoffs:
            self.print_payoffs()
            print()
        return payoffs[0]