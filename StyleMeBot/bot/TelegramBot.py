#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import telebot,io
from io import BytesIO
import sys
from PIL import Image
sys.path.append('../')
from model.utils.model_runner import *
bot_token="1241698138:AAHgnnQNH7FaaWmXv3B_uY0NdAnrDjdJld4"
IMAGE_PATH='../data/images'
MODEL_PATH = '../model/model'
model_name = 'vgg19-dcbb9e9d.pth'
image_list=[]
ask_name=True
bot = telebot.TeleBot(bot_token)
keyboard1 = telebot.types.ReplyKeyboardMarkup()
keyboard1.row('Так', 'Ні')



@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, 'Вітаю! Як тебе звати?')

@bot.message_handler(content_types=['text'])
def send_text(message):
    if message.text.lower() == 'так':
        bot.send_message(message.chat.id, 'Ти за адресою. Надішли, будь ласка, фото, яке хочеш стилізувати.')
    elif message.text.lower() == 'ні':
        bot.send_message(message.chat.id, 'Сподіваюсь, ти передумаєш. Чекатиму на тебе!')
    else:
        name = message.text
        bot.send_message(message.chat.id,"Приємно познайомитись, %s. Хочеш створити стилізоване фото?" % name, reply_markup=keyboard1)
        
@bot.message_handler(content_types=['photo'])
def handle_message(message):
    if len(image_list)==0:
        file_id_img=message.photo[-1].file_id
        file = bot.get_file(file_id_img)
        content_img=bot.download_file(file.file_path)
        src=IMAGE_PATH+'/'+file.file_path
        image_list.append(src)
        with open(src, 'wb') as new_file:
            new_file.write(content_img)
        bot.send_message(message.chat.id,"Надішли стиль, в якому хочеш побачити своє фото.")
    else:
        st = StyleTransfer()
        
        file_id_img=message.photo[-1].file_id
        file = bot.get_file(file_id_img)
        image_=bot.download_file(file.file_path)
        src=IMAGE_PATH+'/'+file.file_path
        image_list.append(src)
        with open(src, 'wb') as new_file:
            new_file.write(image_)
        style_img = Image.open(image_list.pop())
        content_img=Image.open(image_list.pop())
        bot.send_message(message.chat.id,"Зачекай хвилинку...")
        output_image = st.transform(content_img, style_img, f'{MODEL_PATH}/{model_name}')
        bot.send_message(message.chat.id,"Готово! Як тобі результат? Подобається?")
        bio = BytesIO()
        bio.name = 'image.jpeg'
        output_image.save(bio, 'JPEG')
        bio.seek(0)
        bot.send_photo(message.chat.id, photo=bio)

        
    

@bot.message_handler(content_types=['sticker'])
def sticker_id(message):
    print(message)

bot.polling()


# In[ ]:





# In[ ]:




